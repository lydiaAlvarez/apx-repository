package com.bbva.qwai.lib.r001.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bbva.qwai.dto.customers.CustomerDTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The  interface QWAIR001Impl class...
 */
public class QWAIR001Impl extends QWAIR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001Impl.class);

	/**
	 * The execute method...
	 */
	@Override
	public List<CustomerDTO> execute() {

		List<CustomerDTO> listDTO = new ArrayList<>();

		CustomerDTO cust1 = new CustomerDTO();
		CustomerDTO cust2 = new CustomerDTO();

		Date date = new Date();

		// First cust1
		cust1.setCustomerId("00001");
		cust1.setFirstName("Jhon");
		cust1.setLastName("Freeman");
		cust1.setNationality("EN");
		cust1.setPersonalTitle("Sr");
		cust1.setGenderId("Male");
		cust1.setIdentityDocumentType("1");
		cust1.setIdentityDocumentNumber("12345678N");
		cust1.setBirthDate(date );
		cust1.setMaritalStatus("Single");

		// Second cust2
		cust2.setCustomerId("00002");
		cust2.setFirstName("Juan");
		cust2.setLastName("Perez");
		cust2.setNationality("ES");
		cust2.setPersonalTitle("Sr");
		cust2.setGenderId("Male");
		cust2.setIdentityDocumentType("1");
		cust2.setIdentityDocumentNumber("98765432N");
		cust2.setBirthDate(date);
		cust2.setMaritalStatus("Single");

		listDTO.add(cust1);
		listDTO.add(cust2);

		return listDTO;
	}
}
